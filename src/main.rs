extern crate regex;

use std::process::exit;
use std::env;
use std::fs::File;
use std::io::{Read, Write};
use std::error::Error;
use regex::Regex;


fn main() {
    let args: Vec<String> = env::args().skip(1).collect();    
    
    if args.is_empty() {
        println!("usage: cleansub SRT_FILE");
        exit(0);
    }
    
    let sub_file = &args[0];
    let text = load_text_file(&sub_file);    
    
    match text {     
        Ok(text) => {
            /*
            Match 1 (<.*?>): start html tags <...>
            Match 2 (\d{2}:\d{2}:\d{2},\d{3}): xx:xx:xx,xxx numbers
            Match 3 (<\\/.*?>): enclosed tags </...>
            Match 4 (-->): -->
            Match 5 (\d{1,}\n): any number followed by new line (\n)
            Match 6 (<\\/.*?>): end html tags
            Match 7 (\n{2}?): double newline characters \n\n
            Match 8: (\[.*\]): match [ or ] with characters inside
            */
            let re = Regex::new(r"<.*?>|\d{2}:\d{2}:\d{2},\d{3}|<\\/.*?>|-->|\d{1,}\n|\n{2}?|\[.*\]")
                        .expect("Regular expresion is not correct");
            let clean_text = re.replace_all(&text, "");    
            
            println!("Saving clean subtitles...");            
            
            match save_text_file(&sub_file, &clean_text.to_string()) {
                Ok(_) => println!("File saved!"),
                Err(err) => println!("Could not save clean sub. file: {}", err.description())
            }
        },
        Err(ref e) => {        
            println!("Error when reading file: {:?}", e.description());            
        }
    }
}

fn load_text_file(fname: &String) -> Result<String, Box<Error>> {
    let mut contents = String::new();    
    File::open(fname)?.read_to_string(&mut contents)?;
    Ok(contents)
}

fn save_text_file(fname: &String, data: &String) -> Result<(), Box<Error>> {
    let new_name: Vec<&str> = fname.split(".").collect();
    let mut f = File::create(format!("{}_new.srt", new_name[0]))?;
    Ok(f.write_all(data.as_bytes())?)
}
