# cleansub

Utility (created with Rust language) for removing time marks and html tags from .srt file

# Installation

1. cd cleansub
2. cargo run

# Usage

1. Get inside target/debug
2. Execute: cleansub <SRT_FILE>
